from bs4 import BeautifulSoup
import re
import lemme as lem
import json
import spacy
import glob ###--> liste des fichiers


def Candidat(nom):#Permet d'avoir une meilleur présentation Prenom Nom sans problème
    tabCandidat = ["M Le Pen","E Zemmour","J-L Mélenchon","J-L Mélenchon","V Pécresse","V Pécresse","J Lassalle","Y Jadot","A Hidalgo","N Dupont-Aignan","N Arthaud","P Poutou","F Roussel","E Macron"]
    tabNom = ["le pen","zemmour","mélenchon","melenchon","pécresse","pecresse","lassalle","jadot","hidalgo","dupont aignan","arthaud","poutou","roussel","macron"]#en lower car parfois il y a soit LE PEN soit le pen, donc pour eviter les problemes de MAJ et de low
    return tabCandidat[tabNom.index(nom.lower())]


def date_Nbmots_categorie(mots,BDD):#mots (notre phrase découpé par virgule), BDD (le dictionnaire pour notre article)
    jours = ["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"]       
    if(len(mots)!=1):#Si il n'y a pas que la date
        for i in range(0,len(mots)):#Pour toutes les informations dans le tableau
            if(mots[i].split()[0] in jours):#On regarde si c'est la date si oui alors
               BDD["Catégorie"]=mots[i-1]#L'information avant la date est la catégorie
               infos = mots[i].split()
               BDD["Date"]=infos[0]+" "+infos[1]+" "+infos[2]+" "+infos[3]#On ajoute date,mois,année
               BDD["Nbmots"]=int(infos[-2])#On ajoute le nombre de mots
               break
    else:#Il y a que la date
        infos = mots[0].split()#Recupere la date et le nombre de mot en liste
        #print(infos)
        if(infos[0] in jours):
            BDD["Date"]=infos[0]+" "+infos[1]+" "+infos[2]+" "+infos[3]#Récupere le jour , la date, le mois, l'annee
            BDD["Nbmots"]=int(infos[-2])#Recupere le nombre de mots
        else:#Toutes les informations sont dans un seul bloque
            BDD["Date"]="None"
            BDD["Nbmots"]=0 #0 car ce n'est pas indiqué

def nomJournal_Geo(doc,BDD):#doc(modele spacy fait sur le texte), BDD (le dictionnaire pour notre article)
        mots=""
        if "  " in doc.text.strip():#Si il y a 2 espaces alors on récupere le début, car il y a 2 espaces entre le nom du journal et la géographie La Dépêche du Midi  GÉNÉRAL
            mots = doc.text.strip().split("  ")#On sépare notre texte en plusieur partie entre chaque double espace
        else:#Sinon 
            mots = doc.text.strip().split(",")#On sépare notre texte en plusieurs partie entre chaque virgule
        
        if(mots[0].find('(') != -1): #Si il y a une parentheses cela signifie la plupart du temps que c'est une localisation, donc on sépare, la 1ere partie est le nom du journal, le deuxieme la localisation Le Progrès (Lyon) par exemple
            mots2 = mots[0].split("(")
            BDD["NomJournal"]=mots2[0]
            BDD["Géographie"]=mots2[1]#Peut etre enlever Géographie? pas vraiment d'interet
            
        else:
            #nomJournal = mots[0].split(";") #Pour empecher tout les noms de départements
            BDD["NomJournal"]=mots[0] #On récupère le nom du journal car il est toujours en première position

def ajout_Entite_nommee(modele,Entite_Nommee):
    for entite in modele.ents:
            #print(entite.text + ' | '+ entite.label_)
            Entite_Nommee.append(entite.text)

def Candidat_citee(texte,candidats,compteur):
    liste_candidats = ["le pen","zemmour","mélenchon","melenchon","pécresse","pecresse","lassalle","jadot","hidalgo","dupont aignan","arthaud","poutou","roussel","macron"]#Tableau des candidats en lower() pour eviter les LE PEN 
    for i in range (0,len(texte)):#Recherche de candidat dans le titre avec la lemmatisation pour eviter les noms avec une virgule coller, parfois le candidat est le dernier mot et du coup si on prend len(texte)-1 on a pas le nom du candidat
        if texte[i].lower() in liste_candidats:#Pour tout les candidats sauf Le Pen
            if Candidat(texte[i]) not in candidats:
                candidats.append(Candidat(texte[i]))     
        if texte[i-1].lower()+" "+texte[i].lower() in liste_candidats:#Pour trouver Le Pen car c'est en 2 mots,-1 car parfois le candidat est le dernier mot et du coup si on prend len(texte)-1 on a pas le nom du candidat
            if Candidat(texte[i-1]+" "+texte[i]) not in candidats:#On regarde si le candidat n'est pas déjà trouvé
                candidats.append(Candidat(texte[i-1]+" "+texte[i]))#Si non, alors on le rajoute

def Candidat_Vide(texte):
    liste_candidats = ["le pen","zemmour","mélenchon","melenchon","pécresse","pecresse","lassalle","jadot","hidalgo","dupont aignan","arthaud","poutou","roussel","macron"]#Tableau des candidats en lower() pour eviter les LE PEN 
    for i in range (0,len(texte)-1):
        print(texte[i])
        if texte[i].lower() in liste_candidats:#Pour tout les candidats sauf Le Pen
            print("oui")
        if texte[i].lower()+" "+texte[i+1].lower() in liste_candidats:#Pour trouver Le Pen car c'est en 2 mots
            print("oui3")

def Creation_corpus(lien,compteurs,journal):#compteur pour la clé dans journaux (plusieurs html)
    with open(lien,"r") as f:
        contenu = f.read()
        journaux = journal
        soup = BeautifulSoup(contenu,'html.parser')
        articles = soup.findAll("article")
        compteur = compteurs
    
        for article in articles:
            #journaux.setdefault(compteur,{})
            #Initialisation de la BDD pour chaque Article
            BDD = {}
            BDD.setdefault("Date","")
            BDD.setdefault("Nbmots",0)
            BDD.setdefault("NomJournal","")
            BDD.setdefault("Géographie","")
            BDD.setdefault("Catégorie","None")
            BDD.setdefault("Candidat","")
            BDD.setdefault("Contenu","")
            BDD.setdefault("Lemmatiser","")
            BDD.setdefault("Entite_Nommee","")

            #Debut de l'analyse
            print(compteur)
            """La date, le nombre de mot, et la catégorie de l'article"""
            DocHead = article.find('span',class_='DocHeader')#Recupere le span avec la class DocHeader (pour la date, le nombre de mot, et la catégorie de l'article)
            #print(a)
            #print(a.text.strip())
            mots = DocHead.text.strip().split(',')#Recupere le texte et le separe en liste de mot
            #print(DocHead.text)
            date_Nbmots_categorie(mots,BDD)

            """Le nom du journal et la géographie"""
            modele_LG = spacy.load("/export/spacy-libs/fr_core_news_lg/fr_core_news_lg-3.1.0")
            DocPublication = article.find('span',class_='DocPublicationName')#Recupere le span avec la class DocPublication (pour le nom du journal)
            #print(DocPublication.text.strip())
            doc = modele_LG(DocPublication.text)
            nomJournal_Geo(DocPublication,BDD)#On ajoute le nom du journal dans la BDD

            """Les candidats, le contenu et les entites nommees"""
            #liste_candidats_spaCy = ["Marine","Marine Le Pen","Le Pen","Eric","Eric Zemmour","Zemmour","Jean-Luc","Jean-Luc Mélenchon","Mélenchon","Valérie","Valérie Pécresse","Pécresse","Jean","Jean Lassalle","Lassalle","Yannick","Yannick Jadot","Jadot","Anne","Anne Hidalgo","Hidalgo","Nicolas","Nicolas Dupont-Aignan","Dupont-Aignan","Nathalie","Nathalie Arthaud","Arthaud","Philippe","Philippe Poutou","Poutou","Fabien","Fabien Roussel","Roussel","Emmanuel","Emmanuel Macron","Macron"]
            Entite_Nommee = []#Entites nommees dans l'article
            candidats =[]#Les candidats cités 
            #Titre
            titre = article.find('div',class_="titreArticle")#récuperer la div où est le text
            """Nom = titre.findAll('mark')
            for i in Nom:
                print(i.text)"""
            titre_utilisable = re.sub('\s+',' ',titre.text)#Retire les doubles espaces autour des candidats
            lem_titre = lem.lemme(lem.tokenlem(titre_utilisable))
            #Modele Lg
            modele_LG = spacy.load("/export/spacy-libs/fr_core_news_lg/fr_core_news_lg-3.1.0")
            #LG
            document = modele_LG(titre_utilisable)
            ajout_Entite_nommee(document,Entite_Nommee) #Entite nommee dans le titre
            #Titre Sans spaCy
            Candidat_citee(lem_titre,candidats,compteur)

            #Modele Sm
            """modele_SM = spacy.load("/export/spacy-libs/fr_core_news_sm/fr_core_news_sm-3.1.0")"""
            #Titre avec spaCy
            #documenttest = modele_LG(' '.join(lem_titre))#' '.join() permet de transformer un tableau de string en string avec ' ' entre chaque mots
            ajout_Entite_nommee(document,Entite_Nommee) #Entite nommee dans le titre
            #Sm

            #Texte
            texte = article.find("div",class_='DocText clearfix')#Recuperer le texte
            texte_utilisable = re.sub('\s+',' ',texte.text)#Retire les doubles espaces autour des candidats
            lem_texte = lem.lemme(lem.tokenlem(texte_utilisable))#lower car parfois certains mettent le nom des candidats en majuscule donc ca n'affiche pas
            #print(lem.lemme(lem.tokenlem(texte.text.strip())))
            #mots4 = lem_texte
            #Texte avec spaCy
            document2 = modele_LG(texte_utilisable)
            ajout_Entite_nommee(document2,Entite_Nommee)#Entite nommee dans le texte

            #Texte sans spaCy
            Candidat_citee(lem_texte,candidats,compteur)
            if(len(candidats)!=0):#Je pars du principe que je trouve tous les candidats si je n'en trouve pas c'est qu'il n'y en a pas
                #Si le titre est le contenu et du coup il n'y en a pas
                if len(texte.text.strip())==0: 
                    BDD["Contenu"]=titre_utilisable#Ajout du titre dans le contenu
                    BDD["Lemmatiser"]=' '.join(lem_titre)

                #Si il y a un contenu
                else:
                    BDD["Contenu"]="titre: "+titre_utilisable+" texte: "+texte_utilisable#On indique le titre et le texte
                    BDD["Lemmatiser"]="titre "+ ' '.join(lem_titre)+" texte: "+' '.join(lem_texte) #On ajoute le titre et le texte lemmatisés

                #Ajout des candidats dans la BDD
                for i in candidats:
                    if i == candidats[-1]:
                        BDD["Candidat"]+=i
                    else:
                        BDD["Candidat"]+=i+","

                #On ajoute les entites nommees
                BDD["Entite_Nommee"]=Entite_Nommee
                #print(BDD)
                """Ajout de la BDD de l'article dans le dictionnaire """        
                journaux[compteur]=BDD
            else:#Il n'y a pas de candidat on affiche le texte pour être sur qu'il y a bien aucun candidat
                print("VIDE",compteur)
                Candidat_Vide(lem_titre)
                Candidat_Vide(lem_texte)
            compteur+=1
    return journaux

#Main
liste_corpus = glob.glob("Articles/corpus2/HTML/*")
Journaux = {}
Journaux = Creation_corpus("Articles/corpus1/biblioeuropresse20220407091757.HTML",len(Journaux),Journaux)
for chemin in liste_corpus:
    bot = Creation_corpus(chemin,len(Journaux),Journaux)
    Journaux = bot

#Ecrire dans le fichier json les résultats
with open("Articles/corpus1/corpus.json","w",encoding="utf-8") as w:
    w.write(json.dumps(Journaux, indent=2, ensure_ascii=False))

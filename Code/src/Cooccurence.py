import json
import xlsxwriter
import spacy
from spacy.tokens import Doc, Token, Span

with open("Articles/corpus2/corpus.json","r",encoding="utf-8") as c:
     corpus = json.load(c)#On ouvre notre json
     liste_candidats = ["J-L Mélenchon","N Arthaud","P Poutou","F Roussel","Y Jadot","A Hidalgo","E Macron","J Lassalle","V Pécresse","N Dupont-Aignan","M Le Pen","E Zemmour"]
     cooccurence_candidat = {}#dictionnaire pour les resultats globaux
     Paire_candidat = {}#dictionnaire pour les resultats de paire de candidat
     Candidat_solo = {}#dictionnaire pour les resultats des candidat cites seuls
     Corpus_Solo = {}#dictionnaire pour recuperer les textes ou les candidats sont cites seul

     """Cooccurence entre candidat"""
     for i in liste_candidats:#Construction du dico de cooccurence de chaque candidat avec tout les autres et le nombre d'apparition entre eux
          #Construction des dicos avec tous les candidats
          dico_candidats = {}
          dico_candidats2 = {}
          dico_candidats3 = {}
          for candidat in liste_candidats:#Construction du dico comportant tout les candidats et initie leurs compteur à 0
               #Ajout des candidats
               dico_candidats.setdefault(candidat,0)
               dico_candidats2.setdefault(candidat,0)
               dico_candidats3.setdefault(candidat,0)
          #Inialisation des dicos de résultats
          cooccurence_candidat.setdefault(i,dico_candidats)
          Paire_candidat.setdefault(i,dico_candidats2)
          Candidat_solo.setdefault(i,dico_candidats3)
          Corpus_Solo.setdefault(i,[])

     """Tous les candidats"""
     for i in corpus:#On regarde les candidats cites ensemble
          print(i)
          candidat_cites = corpus[i]["Candidat"].split(",")
          for candidat1 in candidat_cites:#Tous les candidats cites dans l'articles
               for candidat2 in candidat_cites:#Tous les candidats cites dans l'articles
                    cooccurence_candidat[candidat1][candidat2]+=1#On ajoute +1 pour les candidats cites ensemble
                    
     """Paire de candidat, Candidat cités seul"""
     for i in corpus:
          #Initialisation
          BDD = {}
          BDD.setdefault("Date","")
          BDD.setdefault("Numero de l'article","")
          BDD.setdefault("Texte","")
          BDD.setdefault("Lemmatiser","")
          BDD.setdefault("Entite_Nommee","")
          candidat_cites = corpus[i]["Candidat"].split(",")#On recupere sous forme de tableau les candidats 
          if len(candidat_cites) == 2:#Paire de candidats
               #On ajoute au deux endroits 
               Paire_candidat[candidat_cites[0]][candidat_cites[1]]+=1
               Paire_candidat[candidat_cites[1]][candidat_cites[0]]+=1
          if len(candidat_cites) == 1:#Candidat cités seul
               #On ajoute dans la case du candidat avec lui-meme
               Candidat_solo[candidat_cites[0]][candidat_cites[0]]+=1
               BDD["Date"]+=corpus[i]["Date"]#Recupere la date de l'article
               BDD["Numero de l'article"]+=i#Recupere le numero de l'article
               BDD["Texte"]+=corpus[i]["Contenu"]#Recupere le texte
               BDD["Lemmatiser"]+=corpus[i]["Lemmatiser"]#Recupere le texte lemmatiser
               BDD["Entite_Nommee"]=corpus[i]["Entite_Nommee"]#Recupere les entites nommées
               Corpus_Solo[candidat_cites[0]].append(BDD)
               #Faire un csv  numero de l'article(header), nom du candidat(par ligne) schema 1

     #########################Global##################################
     """Enregistrement des résultats en json Global"""
     with open("Articles/corpus2/cooccurenceCandidat.json","w",encoding="utf-8") as file1:
         file1.write(json.dumps(cooccurence_candidat, indent=2, ensure_ascii=False))

     """Transformation des résultats en tableau Excel"""   
     workbook  = xlsxwriter.Workbook('Articles/corpus2/resultatCooccurence.xlsx')
     worksheet = workbook.add_worksheet()
     for x in range (0,len(cooccurence_candidat)):
          worksheet.write(0, x+1, liste_candidats[x])
          worksheet.write(x+1,0,liste_candidats[x])
     for candidat1 in liste_candidats:
          for candidat2 in liste_candidats:
               worksheet.write(liste_candidats.index(candidat1)+1,liste_candidats.index(candidat2)+1,cooccurence_candidat[candidat1][candidat2])
     workbook.close()
     file1.close()
     ############################################################################

     ##################################Paire########################################
     """Enregistrement des résultats en json Paire"""
     with open("Articles/corpus2/PaireCandidat.json","w",encoding="utf-8") as file2:
         file2.write(json.dumps(Paire_candidat, indent=2, ensure_ascii=False))

     

     """Transformation des résultats en tableau Excel"""   
     workbook  = xlsxwriter.Workbook('Articles/corpus2/resultatPaire.xlsx')
     worksheet = workbook.add_worksheet()
     for x in range (0,len(Paire_candidat)):
          worksheet.write(0, x+1, liste_candidats[x])
          worksheet.write(x+1,0,liste_candidats[x])
     for candidat1 in liste_candidats:
          for candidat2 in liste_candidats:
               worksheet.write(liste_candidats.index(candidat1)+1,liste_candidats.index(candidat2)+1,Paire_candidat[candidat1][candidat2])
     workbook.close()
     file2.close()
     ##############################################################################
     
     ####################################Resultat des candidats cites seuls#############################
     """Enregistrement des résultats en json Solo"""
     with open("Articles/corpus2/Candidat_seul.json","w",encoding="utf-8") as file3:
         file3.write(json.dumps(Candidat_solo, indent=2, ensure_ascii=False))

     """Transformation des résultats en tableau Excel"""   
     workbook  = xlsxwriter.Workbook('Articles/corpus2/resultatSeul.xlsx')
     worksheet = workbook.add_worksheet()
     for x in range (0,len(Candidat_solo)):
          worksheet.write(0, x+1, liste_candidats[x])
          worksheet.write(x+1,0,liste_candidats[x])
     for candidat1 in liste_candidats:
          for candidat2 in liste_candidats:
               worksheet.write(liste_candidats.index(candidat1)+1,liste_candidats.index(candidat2)+1,Candidat_solo[candidat1][candidat2])
     workbook.close()
     file2.close()
     #########################################################################################################

     #############################################Corpus des candidats cites seuls##############################
     """Enregistrement des résultats en json Corpus Solo"""
     with open("Articles/corpus2/Corpus_seul.json","w",encoding="utf-8") as file4:
         file4.write(json.dumps(Corpus_Solo, indent=2, ensure_ascii=False))
     ###########################################################################################################
          

#encoding UTF-8
from bs4 import BeautifulSoup
from nltk.corpus import stopwords
import lemme as lem
import json
import spacy

def Rappel(tab_humain,tab_algo):#Calcul du Rappel
    cVrai = 0
    cFauxN= 0
    for entite in tab_humain:#Pour toute les entités que j'ai repéré
        if entite in tab_algo:#Si le modele a bien trouvé alors +1 Vrai Positif
            cVrai += 1 
        else:#Si j'ai trouvé une entité mais que lui non
            cFauxN += 1 #+1 Faux Negatif
    return cVrai/(cVrai+cFauxN)

def Precision(tab_humain,tab_algo):#Calcul de la Precision
    cVrai = 0
    cFauxP = 0
    for entite in tab_algo:#Pour toutes les entités que le modèle a trouvé
        if entite in tab_humain:#Si je l'ai trouvé aussi 
            cVrai += 1 #+1 Vrai Positif
        else:#Si il a trouvé une entité que je considère comme faux 
            cFauxP += 1 # +1 Faux Positif
    return cVrai/(cVrai+cFauxP)
    
def Fmesure(Rappel,Precision):#Calcul de F-mesure
    return 2*((Precision*Rappel) / (Precision + Rappel))

def Calculs(tab_humain,tab_algo):#Definition de tous les calculs
    ResultatRappel = Rappel(tab_humain,tab_algo)
    print("Rappel",  ResultatRappel)
    ResultatPrecision = Precision(tab_humain,tab_algo)
    print("Précision", ResultatPrecision)
    return Fmesure(ResultatRappel,ResultatPrecision)

def RecupEntiteODT(chemin):#Recuperation des entites fait manuellement
    with open(chemin,"r") as f:#Ouvre le fichier
        listeHumain = []
        contenu = f.read()#Lit le contenu
        soup = BeautifulSoup(contenu,'html.parser')#va dans la balise html.parser
        entiteNommee = soup.findAll("text:span",{"text:style-name":"Entites_20_nommees"})#Recherche toutes les entités nommées
        for entite in entiteNommee:
            listeHumain.append(entite.text)#Ajoute dans la liste
        return listeHumain#Renvoie la liste

def RecupEntiteAlgo(modele,texte):#Permet de récupérer les entités nommées des modèles
    doc = modele(texte.text)#application du modele sur le texte
    liste=[]#Initialisation de la liste des entités
    for entite in doc.ents:
        liste.append(entite.text)#On ajoute l'entité nommée
    return liste

def AjoutDansListeComplete(liste1,liste2,listecomplete):#Permet de récupérer les entités nommées du titre, du texte  et de la liste complete dans une seule liste
    for i in liste1:
        listecomplete.append(i)
    for j in liste2:
        listecomplete.append(j)
    return listecomplete

with open("Articles/corpus1/biblioeuropresse20220407091757.HTML","r") as f:
    contenu = f.read()
    soup = BeautifulSoup(contenu,'html.parser')
    articles = soup.findAll("article")
    compteur = 0
    listCompleteEntiteLg = []#Liste complete des entites nommees de l'algo Lg
    listCompleteEntiteSm = []#Liste complete des entites nommees de l'algo Sm
    for article in articles:
        if(compteur==88 or compteur==86 or compteur==58 or compteur==0 or compteur==6 or compteur==37 or compteur == 42 or compteur==15):
            print(compteur)
            titre = article.find('p',class_='titreArticleVisu rdp__articletitle')
            texte = article.find('div',class_='docOcurrContainer')#Recuperer le texte

            #Modele LG
            #modele_LG = spacy.load("/export/spacy-libs/fr_core_news_lg/fr_core_news_lg-3.1.0")
            modele_LG = spacy.load("fr_core_news_lg")
            listeTemporaireLg = listCompleteEntiteLg #Permet d'ajouter au fur et a mesure 
            print("lg")
            print("titre")
            listeLgTitre = RecupEntiteAlgo(modele_LG,titre)#Recupère la liste des entités nommées du titre
            print("texte")
            listeLgTexte = RecupEntiteAlgo(modele_LG,texte)#Recupère la liste des entités nommées du texte
            listCompleteEntiteLg = AjoutDansListeComplete(listeLgTitre,listeLgTexte,listeTemporaireLg)#Recupère la liste des entités nommées 
            #print("Lg ",len(listCompleteEntiteLg))

            #Modele SM
            #modele_SM = spacy.load("/export/spacy-libs/fr_core_news_sm/fr_core_news_sm-3.1.0")
            modele_SM = spacy.load("fr_core_news_sm")
            listeTemporaireSm = listCompleteEntiteSm
            print("sm")
            print("titre")
            listeSmTitre = RecupEntiteAlgo(modele_SM,titre)
            print("texte")
            listeSmTexte = RecupEntiteAlgo(modele_SM,titre)
            listCompleteEntiteSm = AjoutDansListeComplete(listeSmTitre,listeSmTexte,listeTemporaireSm)
            #print("Sm ",len(listCompleteEntiteSm))

        compteur+=1

    #Affichage de toutes les entites nommees
    print("LG Modele \n",listCompleteEntiteLg)
    print(len(listCompleteEntiteLg))
    print("SM Modele \n",listCompleteEntiteSm)
    print(len(listCompleteEntiteSm))

    #Recuperation des entites fait à la main
    tab_humain= RecupEntiteODT("Rappel_Precision (copie)/content.xml")
    print("Humain \n")
    print(tab_humain)
    print("Récuperer a la main \n",len(tab_humain))

    #Calculs
    #Lg
    print("F-mesure de Lg ",Calculs(tab_humain,listCompleteEntiteLg))
    #Sm
    print("F-mesure de Sm ",Calculs(tab_humain,listCompleteEntiteSm))
    

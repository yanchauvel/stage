import json

with open("Articles/corpus1/Corpus_seul.json","r",encoding="utf-8") as fichier:
    Corpus_seul = json.load(fichier)#Ouverture du json
    Matrice = {}#Initialisation de la matrice 
    Resultat = {}#Initialisation de la matrice des résultats
    for candidat in Corpus_seul:#Pour chaque candidat
        Entite = {}
        for liste in Corpus_seul[candidat]:#Pour toutes les entités nommées du candidats
            for entite in liste["Entite_Nommee"]:
                if entite in Entite:#Si on a déjà repéré l'entité nommée
                    Entite[entite]+=1#On lui rajoute +1
                else:#Si on ne la pas encore rencontré
                    Entite.setdefault(entite,1)#On l'ajoute a nos entité rencontré
        #print(Entite)
        Matrice[candidat]=Entite#On l'ajoute pour notre candidat
        
    #https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value
    for cle in Matrice:#Permet de recuperer les entites nommees par les plus cites au moins cites
        organiser = sorted(Matrice[cle].items(), key=lambda x: x[1],reverse=True)
        Resultat[cle]=organiser
    
    """Enregistrement des résultats en json Global"""
    with open("Articles/corpus1/Analyse_lexical.json","w",encoding="utf-8") as file1:
        file1.write(json.dumps(Resultat, indent=2, ensure_ascii=False))

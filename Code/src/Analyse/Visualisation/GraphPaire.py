import matplotlib.pyplot as plt
import networkx as nx
import json

#https://networkx.org/documentation/stable/auto_examples/basic/plot_simple_graph.html#sphx-glr-auto-examples-basic-plot-simple-graph-py

with open("Articles/corpus1/PaireCandidat.json","r",encoding="utf-8") as fichier1:
    cooc=json.load(fichier1)#Recuperation du json
    liste = []
    
    G = nx.Graph()
    #Pour récupérer les candidats
    for candidat1 in cooc:#Pour chaque candidat
        for candidat2 in cooc[candidat1]:
            if(candidat1 != candidat2 and candidat2 not in liste):#
                G.add_edge(candidat1, candidat2, weight=cooc[candidat1][candidat2])
        liste.append(candidat1)

    pos = nx.spring_layout(G, seed=7)  # positions for all nodes - seed for reproducibility
    nx.draw_networkx_nodes(G, pos, node_size=700)#Rond

    #Large nombre d'article
    """elarge = [(u, v) for (u, v, d) in G.edges(data=True) if d["weight"] >= 50]
    emedium = [(u, v) for (u, v, d) in G.edges(data=True) if d["weight"] > 10 and d["weight"] < 50]
    esmall=[(u, v) for (u, v, d) in G.edges(data=True) if d["weight"] <= 10 and d["weight"]>= 1]
    enull = [(u, v) for (u, v, d) in G.edges(data=True) if d["weight"] == 0]"""

    """nx.draw_networkx_edges(G, pos, edgelist=elarge, width=1, label='>=50')#ligne noire
    nx.draw_networkx_edges(G, pos, edgelist=emedium, width=1, alpha=0.5, edge_color="blue",label=">10 et < 50")#ligne bleu
    nx.draw_networkx_edges(G, pos, edgelist=esmall, width=1, alpha=0.5, edge_color="grey",style = "dashed",label="=>1 et <=10")#ligne bleu
    nx.draw_networkx_edges(G, pos, edgelist=enull, width=1, edge_color="white", label='0')"""

    #Petit nombre d'article
    elarge = [(u, v) for (u, v, d) in G.edges(data=True) if d["weight"] == 2]
    esmall=[(u, v) for (u, v, d) in G.edges(data=True) if d["weight"]== 1]

    nx.draw_networkx_edges(G, pos, edgelist=elarge, width=1, label='2')#ligne noire
    nx.draw_networkx_edges(G, pos, edgelist=esmall, width=1, alpha=0.5, edge_color="blue", style = "dashed",label="1")#ligne bleu
    
    nx.draw_networkx_labels(G, pos, font_size=20, font_family="sans-serif")#Titre

    #edge_labels = nx.get_edge_attributes(G,"weight")#valeur
    #nx.draw_networkx_edge_labels(G, pos, edge_labels)

    ax = plt.gca()
    ax.margins(0.01)#espace entre les points
    ax.legend()
    ax.set_title('Cooccurence des paires de candidat')
    plt.axis("off")
    plt.tight_layout()
    plt.savefig('Articles/corpus1/PaireCandidat.png')
    plt.show()
    
fichier1.close()

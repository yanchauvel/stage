import matplotlib.pyplot as plt
import numpy as np
import json

    
with open("Articles/corpus2/cooccurenceCandidat.json","r",encoding="utf-8") as fichier2:
    cooc2=json.load(fichier2)#Recupération du json
    candidat=[]#liste des candidats
    resultat={}#Dictionnaire des résultats
    for candida in cooc2:#Pour chaque candidat dans le json
        candidat.append(candida)#On l'ajoute a la liste
        liste=[]
        for candida2 in cooc2[candida]:
            liste.append(cooc2[candida][candida2])#On ajoute le résultat entre les candidats a la liste
        resultat[candida]=liste #On l'ajoute au dictionnaire dans la cle du candidat

    #Création du graph
    #https://matplotlib.org/stable/gallery/lines_bars_and_markers/barchart.html#sphx-glr-gallery-lines-bars-and-markers-barchart-py
    x = np.arange(len(cooc2))
    width=0.07
    fig, ax = plt.subplots()
    #https://stackoverflow.com/questions/59149279/matplotlib-selecting-colors-within-qualitative-color-map
    colors = iter([plt.cm.tab20(i) for i in range(20)])
    for i, c in enumerate(candidat):
        rect = ax.bar(x + (i-6.5)*width, resultat[c], width, label=c, color=[next(colors)])
        ax.bar_label(rect, padding=3)

    ax.set_ylabel('Scores')
    ax.set_title('Articles entre candidats')
    ax.set_xticks(x, candidat)
    ax.legend()

    fig.tight_layout()

    plt.show()
    
fichier2.close()

import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import json

with open("Articles/corpus2/corpus.json","r",encoding="utf-8") as fichier:
    cooc=json.load(fichier)
    candidats = ["J-L Mélenchon","N Arthaud","P Poutou","F Roussel","Y Jadot","A Hidalgo","E Macron","J Lassalle","V Pécresse","N Dupont-Aignan","M Le Pen","E Zemmour"]
    journaux=[]
    ResultatJournaux={}
    resultat=[]
    
    for article in cooc:#AJoute les noms des journaux
        if cooc[article]["NomJournal"] not in journaux:
            journaux.append(cooc[article]["NomJournal"])

    for journal in journaux:#Initialisation du nombre de candidats cité pour chaque journal
        BDD = {}
        for candidat in candidats:
            BDD[candidat]=0
        ResultatJournaux[journal]=BDD
    #ResultatJournaux["La Dépêche du Midi  GÉNÉRAL"]['J-L Mélenchon']+=1
    #print(ResultatJournaux)

    for article2 in cooc:#Ajout du nombre de candidat cité pour chaque journal
        for Candidat in cooc[article2]["Candidat"].split(','):
            #print(Candidat)
            #print(ResultatJournaux[cooc[article2]["NomJournal"]][Candidat])
            ResultatJournaux[cooc[article2]["NomJournal"]][Candidat]+=1

    """Enregistrement des résultats en json"""
    with open("Articles/corpus2/CandidatCiteParJournal.json","w",encoding="utf-8") as file:
         file.write(json.dumps(ResultatJournaux, indent=2, ensure_ascii=False))


    #print(ResultatJournaux)
    #Transformation des résultats en tableau
    #Chiffre
    for journal in ResultatJournaux:
        print(journal)
        liste=[]
        for candidat in ResultatJournaux[journal]:
            print(ResultatJournaux[journal][candidat])
            liste.append(ResultatJournaux[journal][candidat])
        resultat.append(liste)

    #Pourcentage
    """for journal in ResultatJournaux:
        print(journal)
        liste=[]
        nombre_total=0
        for candidat in ResultatJournaux[journal]:
            #print(ResultatJournaux[journal][candidat])
            nombre_total+=ResultatJournaux[journal][candidat]
        for candidat2 in ResultatJournaux[journal]:
            pourcentage = (ResultatJournaux[journal][candidat2]/nombre_total)*100
            liste.append(pourcentage)
        resultat.append(liste)"""

    #Transformation en data
    data = pd.DataFrame(resultat, columns=candidats)#Creer un dataset, avec les résultats en matrice [[]]  et en colonne les candidats

    fig, ax = plt.subplots()
    #Chiffre
    ax = sns.heatmap(data,annot=True, yticklabels=journaux, fmt="d",cmap="Blues")#Ajoute les candidats dans l'ordre inverse pour les lignes
    #Pourcentage
    #ax = sns.heatmap(data,annot=True, yticklabels=journaux, fmt="f", cmap="Blues")#Ajoute les candidats dans l'ordre inverse pour les lignes
    plt.show()#Affiche

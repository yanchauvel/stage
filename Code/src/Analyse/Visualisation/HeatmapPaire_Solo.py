import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import json

with open("Articles/corpus1/PaireCandidat.json","r",encoding="utf-8") as fichier:
    with open("Articles/corpus1/Candidat_seul.json","r",encoding="utf-8") as fichier2:
        cooc=json.load(fichier)
        cooc2=json.load(fichier2)
        candidat=[]
        resultat=[]
        for candida in cooc:#Ajoute les candidats dans candidat
            candidat.append(candida)
        
        inverse = list(reversed(candidat))#Inverse candidat, pour que les résultats soient bien visibles
    
        for candida1 in inverse:#Inverse en premier car il remplie par ligne et non par colonne
            liste=[]
            for candida2 in candidat:#On parcour les candidats dans l'ordre original
                if(candida1==candida2):
                    liste.append(cooc2[candida1][candida2])#On prend les articles ou les candidats sont seuls pour la diagonale
                else:
                    liste.append(cooc[candida1][candida2])
            resultat.append(liste)
        
        #Heatmap : https://seaborn.pydata.org/generated/seaborn.heatmap.html
        data = pd.DataFrame(resultat, columns=candidat)#Creer un dataset, avec les résultats en matrice [[]]  et en colonne les candidats
    
        fig, ax = plt.subplots()
        ax = sns.heatmap(data,annot=True, yticklabels= inverse,fmt="d",cmap="Blues")#Ajoute les candidats dans l'ordre inverse pour les lignes
        plt.show()#Affiche
    

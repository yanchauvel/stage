import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import json

with open("Articles/corpus1/cooccurenceCandidat.json","r",encoding="utf-8") as fichier:
    cooc=json.load(fichier)#Récupération json
    candidat = []
    resultat=[]
    print(cooc)
    for candida in cooc:#Ajoute les candidats dans candidat
        candidat.append(candida)
        
    inverse = list(reversed(candidat))#Inverse candidat, pour que les résultats soient bien visibles
    
    for candida1 in inverse:#Inverse en premier car il remplie par ligne et non par colonne
        liste=[]
        for candida2 in candidat:#On parcour les candidats dans l'ordre original
            liste.append(cooc[candida1][candida2])
        resultat.append(liste)


    #https://seaborn.pydata.org/generated/seaborn.heatmap.html   
    #print(resultat)
    data = pd.DataFrame(resultat, columns=candidat)#Creer un dataset, avec les résultats en matrice [[]]  et en colonne les candidats
    #print(candidat)
    
    fig, ax = plt.subplots()
    ax = sns.heatmap(data,annot=True, yticklabels= inverse,fmt="d",cmap="Blues")#Ajoute les candidats dans l'ordre inverse pour les lignes, fmt permet d'avoir les nombres sans l'ecriture scientifique
    plt.show()#Affiche
    

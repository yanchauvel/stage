import matplotlib.pyplot as plt
import numpy as np
import json

with open("Articles/corpus2/Candidat_seul.json","r",encoding="utf-8") as fichier:
    with open("Articles/corpus2/cooccurenceCandidat.json","r",encoding="utf-8") as fichier2:
        Candidat_seul = json.load(fichier)#Récupération du json des candidats seuls
        Candidat_cooc = json.load(fichier2)#Récupération du json des cooccurrences global
        resultatGlobal = []
        resultatSeul = []
        liste_candidat = []
        #Récupération des candidat
        for candidat in Candidat_seul:
            resultatGlobal.append(Candidat_cooc[candidat][candidat])#Récupération des résultats globaux
            resultatSeul.append(Candidat_seul[candidat][candidat])#Récupération des résultats pour les articles seuls
            liste_candidat.append(candidat)#Ajout a la liste des candidats

        #Création du graphique
        #https://matplotlib.org/stable/gallery/lines_bars_and_markers/bar_stacked.html#sphx-glr-gallery-lines-bars-and-markers-bar-stacked-py
        x = np.arange(len(Candidat_seul))
        width = 0.35
        fig, ax = plt.subplots()

        p1 = ax.bar(liste_candidat, resultatGlobal, width, label="Tous les articles")
        p2 = ax.bar(liste_candidat,resultatSeul, width,label='Cité seul')

        ax.set_ylabel("Nombre d'article")
        ax.set_title('Les articles ou les candidats sont cités seul')
        ax.legend()

        ax.bar_label(p1, label_type='center')
        ax.bar_label(p2, label_type='center')

        plt.show()
    fichier2.close()
fichier.close()

import json
#Permet de compter le nombre de journal dans un corpus
with open("Articles/corpus2/corpus.json","r",encoding="utf-8") as fichier:
    corpus = json.load(fichier)#On ouvre notre fichier json
    journaux = []
    for article in corpus:#Pour tous les articles dans le corpus
        #print(corpus[article]["NomJournal"])
        if(corpus[article]["NomJournal"] not in journaux):#On va chercher le journal de l'article
            journaux.append(corpus[article]["NomJournal"])#On l'ajoute à notre liste
    #Affichage
    print(journaux)
    print(len(journaux))
